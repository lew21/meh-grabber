import requests
import time

assert(requests.get('http://localhost/').json() == {'tasks_url': 'http://localhost/tasks/'})

resp = requests.post('http://localhost/tasks/', json={
    'source_url': 'https://google.com',
})
resp.raise_for_status()
assert(resp.status_code == 201)
assert(resp.headers['Location'].startswith('http://localhost/tasks/'))

location = resp.headers['Location']

err = None
for i in range(1, 5):
    try:
        time.sleep(2*i)
        resp = requests.get(location)
        resp.raise_for_status()

        data = resp.json()
        assert(' Gmail ' in data['text'])
        assert(len(data['images']) >= 1)
        assert(any(image['datatype'] == 'image/png' for image in data['images'].values()))
    except Exception as e:
        print('.')
        err = e
        pass
    else:
        break
else:
    raise err

print('OK!')