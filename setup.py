from setuptools import setup, find_packages


with open('README.md') as f:
	readme = f.read()


setup(
	name="grabber",
	version="0.0.0",
	description="Solution for a random recruitment task",
	long_description=readme,
	author="Linus Lewandowski",
	author_email="linus@lew21.net",
	url="https://gitlab.com/lew21/meh-grabber/",
	keywords="grabber",

	install_requires=[
        'celery>=4,<5',
        'falcon>=1,<2',
        'cassandra-driver',
        'requests',
        'gunicorn',
	],

	packages=find_packages(),
	include_package_data=True,

	zip_safe=True,
	classifiers=[]
)
