from html.parser import HTMLParser
from urllib.parse import urljoin


class Stripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.data = ''
        self.ignore = False

    def handle_starttag(self, tag, attrs):
        if tag in ['style', 'script']:
            self.ignore = True

    def handle_endtag(self, tag):
        if tag in ['style', 'script']:
            self.ignore = False

    def handle_data(self, data):
        if not self.ignore:
            self.data += data


def get_plain_text(html):
    s = Stripper()
    s.feed(html)
    return s.data


class ImageFinder(HTMLParser):
    def __init__(self, base_url):
        super().__init__()
        self.base_url = base_url
        self.images = []

    def handle_starttag(self, tag, attrs):
        if tag == 'img':
            src = dict(attrs).get('src')
            if src:
                self.images.append(urljoin(self.base_url, src))


def get_image_urls(base_url, html):
    f = ImageFinder(base_url)
    f.feed(html)
    return f.images
