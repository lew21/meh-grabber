import unittest

from . import html


class TestHTML(unittest.TestCase):

    def test_get_plain_text(self):
        self.assertEqual(html.get_plain_text(
            '<html><b><xyz>lorem</xyz>ipsum dolor</html>'),
            'loremipsum dolor')

    def test_get_plain_text_with_script(self):
        self.assertEqual(html.get_plain_text(
            '''<html><b><xyz>lorem</xyz><script>ipsum</script> dolor</html>'''),
            'lorem dolor')

    def test_get_plain_text_with_style(self):
        self.assertEqual(html.get_plain_text(
            '''<html><b><xyz>lorem</xyz><style>ipsum</style> dolor</html>'''),
            'lorem dolor')

    def test_get_image_urls(self):
        self.assertEqual(html.get_image_urls('https://example.com',
            '<html><b><img src="lorem">ipsum dolor</html>'),
            ['https://example.com/lorem'])

        self.assertEqual(html.get_image_urls('https://example.com',
            '<html><b><img src="https://example.org/lorem">ipsum dolor</html>'),
            ['https://example.org/lorem'])
