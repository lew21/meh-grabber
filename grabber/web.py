from dataclasses import asdict
from base64 import urlsafe_b64encode, urlsafe_b64decode
from uuid import uuid4
from dataclasses import astuple
import falcon

from .models import Page, Image
from .cassandra import connect as connect_to_cassandra
from .tasks import grab_page


def serialize_task(req, page):
    return {
        'url': f'{req.prefix}/tasks/{page.task_id}/',
        **asdict(page)
    }


def serialize_image(req, image):
    image_id = urlsafe_b64encode(image.source_url.encode("utf-8")).decode("utf-8")
    data = {
        'url': req.prefix + f'/tasks/{image.task_id}/images/{image_id}/',
        **asdict(image)
    }
    del data['content']
    return image_id, data


class Base:
    ALLOW = 'OPTIONS, GET'

    def set_meta_headers(self, resp):
        resp.set_header('Allow', self.ALLOW)

    def on_options(self, req, resp, **kwargs):
        self.set_meta_headers(resp)
        resp.media = {}


class Homepage(Base):
    def on_get(self, req, resp):
        self.set_meta_headers(resp)

        resp.media = {
            'tasks_url': req.prefix + '/tasks/'
        }


class TaskList(Base):
    ALLOW = 'OPTIONS, GET, POST'

    def on_options(self, req, resp):
        self.set_meta_headers(resp)
        resp.media = {
            'POST': 'source_url is required; grab_text and grab_images are optional (default: true)'
        }

    def on_post(self, req, resp):
        self.set_meta_headers(resp)

        id = uuid4().hex
        try:
            page = Page(id, req.media['source_url'], grab_queued=True)
        except KeyError:
            resp.content_type = 'text/plain'
            resp.status = falcon.HTTP_400
            resp.body = 'source_url is required; grab_text and grab_images are optional (default: true)'
            return

        cass = connect_to_cassandra()
        cass.execute(page.SAVE, astuple(page))
        grab_page.delay(page.task_id, page.source_url, req.media.get('grab_text', True), req.media.get('grab_images', True))

        resp.status = falcon.HTTP_201
        resp.set_header('Location', f'{req.prefix}/tasks/{page.task_id}/')
        resp.media = serialize_task(req, page)

    def on_get(self, req, resp):
        self.set_meta_headers(resp)

        cass = connect_to_cassandra()
        pages = [Page(*data) for data in cass.execute(Page.SELECT_ALL)]

        resp.media = {page.task_id: serialize_task(req, page) for page in pages}


class TaskResource(Base):
    def on_get(self, req, resp, id):
        self.set_meta_headers(resp)

        cass = connect_to_cassandra()
        try:
            [page] = [Page(*data) for data in cass.execute(Page.SELECT_BY_TASK_ID, (id,))]
        except ValueError:
            resp.status = falcon.HTTP_404
            return

        images = [Image(*data) for data in cass.execute(Image.SELECT_BY_TASK_ID, (id,))]

        ret = serialize_task(req, page)
        ret['images'] = dict(serialize_image(req, image) for image in images)

        resp.media = ret


class ImagesList(Base):
    def on_get(self, req, resp, id):
        self.set_meta_headers(resp)

        cass = connect_to_cassandra()
        images = [Image(*data) for data in cass.execute(Image.SELECT_BY_TASK_ID, (id,))]
        resp.media = dict(serialize_image(req, image) for image in images)


class ImageResource(Base):
    def on_get(self, req, resp, id, image_id):
        self.set_meta_headers(resp)

        cass = connect_to_cassandra()
        url = urlsafe_b64decode(image_id).decode('utf-8')
        [image] = [Image(*data) for data in cass.execute(Image.SELECT_BY_TASK_ID_AND_SOURCE_URL, (id, url))]

        resp.status = falcon.get_http_status(image.status)
        resp.content_type = image.datatype
        resp.data = image.content


app = falcon.API()
app.add_route('/', Homepage())
app.add_route('/tasks/', TaskList())
app.add_route('/tasks/{id}/', TaskResource())
app.add_route('/tasks/{id}/images/', ImagesList())
app.add_route('/tasks/{id}/images/{image_id}/', ImageResource())


def application(environ, start_response):
    # TODO Delete this line after https://gitlab.com/lew21/apibrowser/issues/1
    if not ('/images/' in environ['PATH_INFO'] and not environ['PATH_INFO'].endswith('/images/')):

        if 'text/html' in environ['HTTP_ACCEPT']:
            # APIBrowser - see https://gitlab.com/lew21/apibrowser
            start_response('200 OK', [('Content-Type', 'text/html; charset=utf-8'), ('Vary', 'Accept')])
            return [b'''<!DOCTYPE html><html><head><meta charset=utf-8><meta name=viewport content="width=device-width,initial-scale=1"><title>APIBrowser</title><link href=https://apibrowser.storage.googleapis.com/static/css/app.568f5941d42567ef2a65c5cdb7b32463.css rel=stylesheet integrity="sha256-wu9idIWIq6GpemTy2UjJyALKwWzhx3asHU+SG2udyEE=" crossorigin=anonymous></head><body><div id=app></div><script type=text/javascript src=https://apibrowser.storage.googleapis.com/static/js/app.9f8edfd9b0e1afb81473.js integrity="sha256-Sdgc+hP8zDdwQwDYypvPePGGs3m7Auqw9Qs9uxOwCoQ=" crossorigin=anonymous></script></body></html>''']

    def start_response_wrapper(status, headers):
        # TODO Delete this replacement after https://gitlab.com/lew21/apibrowser/issues/2
        start_response(status, [('Vary', 'Accept')] + [(name, value.replace('application/json; charset=UTF-8', 'application/json')) for name, value in headers])

    return app(environ, start_response_wrapper)
