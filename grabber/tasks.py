from celery import Celery
from celery.exceptions import Retry
from dataclasses import astuple
import os
import requests
import cassandra

from . import html
from .models import Page, Image
from .cassandra import connect as connect_to_cassandra


RABBITMQ_URL = os.environ.get('RABBITMQ_URL', 'pyamqp://guest@localhost//')

app = Celery('tasks', broker=RABBITMQ_URL)


@app.task(bind=True, autoretry_for=(cassandra.DriverException,), retry_backoff=True)
def grab_page(self, task_id, source_url, grab_text, grab_images):
    page = Page(task_id, source_url)
    cass = connect_to_cassandra()

    resp = requests.get(source_url)
    try:
        page.status = resp.status_code
        resp.raise_for_status()

        if grab_text:
            page.text = html.get_plain_text(resp.text)

        if grab_images:
            image_urls = html.get_image_urls(resp.url, resp.text)
            for image_url in image_urls:
                cass.execute(Image.SAVE, astuple(Image(task_id, image_url, grab_queued=True)))
                grab_image.delay(task_id, image_url)

        page.grab_finished = True

    except requests.RequestException:
        try:
            raise self.retry()
        # Check if this will actually retry the task (ie. we are under the retry limit)
        except Retry:
            page.grab_queued = True
            raise

    finally:
        # No matter what happened, save current status, so users will know about the problems.
        cass.execute(page.SAVE, astuple(page))


@app.task(bind=True, autoretry_for=(cassandra.DriverException,), retry_backoff=True)
def grab_image(self, task_id, source_url):
    image = Image(task_id, source_url)
    cass = connect_to_cassandra()

    resp = requests.get(source_url)
    try:
        image.status = resp.status_code
        resp.raise_for_status()

        image.datatype = resp.headers['content-type']
        image.content = resp.content
        image.grab_finished = True

    except requests.RequestException:
        try:
            raise self.retry()
        # Check if this will actually retry the task (ie. we are under the retry limit)
        except Retry:
            image.grab_queued = True
            raise

    finally:
        # No matter what happened, save current status, so users will know about the problems.
        cass.execute(image.SAVE, astuple(image))
