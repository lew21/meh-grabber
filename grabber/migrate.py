import cassandra
from cassandra.cluster import Cluster

from .cassandra import hosts, port, keyspace
from .models import Page, Image


cluster = Cluster(hosts, port=port)
cass = cluster.connect()

cass.execute(f"""
    CREATE KEYSPACE IF NOT EXISTS {keyspace}
    WITH replication = {{ 'class': 'SimpleStrategy', 'replication_factor': '2' }}
""")

cass.set_keyspace(keyspace)

try:
    cass.execute(Page.CREATE_TABLE)
except cassandra.AlreadyExists:
    pass

try:
    cass.execute(Image.CREATE_TABLE)
except cassandra.AlreadyExists:
    pass
