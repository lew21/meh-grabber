from cassandra.query import SimpleStatement
from dataclasses import dataclass


@dataclass
class Page:
    task_id: str
    source_url: str
    status: int = 0
    text: str = ''
    grab_queued: bool = False
    grab_finished: bool = False

    CREATE_TABLE = SimpleStatement('''
        CREATE TABLE pages (
            task_id text,
            source_url text,
            status int,
            text text,
            grab_queued boolean,
            grab_finished boolean,
            PRIMARY KEY ((task_id))
        )
    ''')

    SAVE = SimpleStatement('''
        INSERT INTO pages (task_id, source_url, status, text, grab_queued, grab_finished)
        VALUES (%s, %s, %s, %s, %s, %s)
    ''')

    SELECT_ALL = SimpleStatement('''
        SELECT task_id, source_url, status, text, grab_queued, grab_finished FROM pages
    ''')

    SELECT_BY_TASK_ID = SimpleStatement('''
        SELECT task_id, source_url, status, text, grab_queued, grab_finished FROM pages WHERE task_id = %s
    ''')


@dataclass
class Image:
    task_id: str
    source_url: str
    status: int = 0
    datatype: str = ''
    content: bytes = b''
    grab_queued: bool = False
    grab_finished: bool = False

    CREATE_TABLE = SimpleStatement('''
        CREATE TABLE images (
            task_id text,
            source_url text,
            status int,
            datatype text,
            content blob,
            grab_queued boolean,
            grab_finished boolean,
            PRIMARY KEY ((task_id), source_url)
        )
    ''')

    SAVE = SimpleStatement('''
        INSERT INTO images (task_id, source_url, status, datatype, content, grab_queued, grab_finished)
        VALUES (%s, %s, %s, %s, %s, %s, %s)
    ''')

    SELECT_BY_TASK_ID = SimpleStatement('''
        SELECT task_id, source_url, status, datatype, content, grab_queued, grab_finished FROM images WHERE task_id = %s
    ''')

    SELECT_BY_TASK_ID_AND_SOURCE_URL = SimpleStatement('''
        SELECT task_id, source_url, status, datatype, content, grab_queued, grab_finished FROM images WHERE task_id = %s AND source_url = %s
    ''')
