import os
from cassandra.cluster import Cluster
from urllib.parse import urlsplit

CASSANDRA_URL = os.environ.get('CASSANDRA_URL', 'cql://localhost/grabber')

url = urlsplit(CASSANDRA_URL)
hosts = url.hostname.split(',')
port = url.port or 9042
keyspace = url.path.lstrip('/')

def connect():
    return Cluster(hosts, port=port).connect(keyspace)
