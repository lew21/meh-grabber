# Grabber
Solution for a random recruitment task.

## Launch the project
```sh
docker-compose up --build
```

You might need to wait a bit for everything to start. Then, open http://localhost/.

## Run unit tests
```sh
docker-compose run web python -m unittest
```

## Run smoke test
```sh
./smoke_test.sh
```

This will launch whole project, create a task, wait for its result, verify it a bit, and shut down the project.

## API structure

### /tasks
#### GET - List all tasks
GET returns all existing tasks:
```json
{
  "49061bad42654b1984a188499f1b81dd": {
    "url": "http://localhost/tasks/49061bad42654b1984a188499f1b81dd/",
    "task_id": "49061bad42654b1984a188499f1b81dd",
    "source_url": "https://google.com/",
    "status": 200,
    "text": "lorem ipsum dolor",
    "grab_queued": false,
    "grab_finished": true
  }
}
```

#### POST - Create a task
Request data:
```json
{
  "source_url": "https://google.com/",
  "grab_text": true,
  "grab_images": true,
}
```
**grab_text** and **grab_images** are optional, default to true.

The call will return 201 Created, with the Location header set to the task URL, and the task info returned in the response body.

### /tasks/ID
#### GET - Check task info
```json
{
  "url": "http://localhost/tasks/49061bad42654b1984a188499f1b81dd/",
  "task_id": "49061bad42654b1984a188499f1b81dd",
  "source_url": "https://google.com/",
  "status": 200,
  "text": "lorem ipsum dolor",
  "grab_queued": false,
  "grab_finished": true,
  "images": {
    "aHR0cHM6Ly93d3cuZ29vZ2xlLnBsL2ltYWdlcy9icmFuZGluZy9nb29nbGVsb2dvLzF4L2dvb2dsZWxvZ29fY29sb3JfMjcyeDkyZHAucG5n": {
      "url": "http://localhost/tasks/49061bad42654b1984a188499f1b81dd/images/aHR0cHM6Ly93d3cuZ29vZ2xlLnBsL2ltYWdlcy9icmFuZGluZy9nb29nbGVsb2dvLzF4L2dvb2dsZWxvZ29fY29sb3JfMjcyeDkyZHAucG5n/",
      "task_id": "49061bad42654b1984a188499f1b81dd",
      "source_url": "https://www.google.pl/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png",
      "status": 200,
      "datatype": "image/png",
      "grab_queued": false,
      "grab_finished": true
    }
  }
}
```
**text** contains the grabbed text, and **url** fields in the **images** array point to the grabbed images.

### /tasks/ID/images/ID
#### GET - Download the image
The image will be served directly, with the correct content type.

## Architecture
All the high-level tasks are split into smaller Celery tasks - one to fetch the source URL, and then multiple ones to fetch the images. Their results are stored in a Cassandra database.

[APIBrowser](https://gitlab.com/lew21/apibrowser) is used to make the API usable from the web browser.

### Why Cassandra?
- We don't need full SQL here
- It's much easier to scale and set up in a HA way than SQL databases
- I'd use ScyllaDB instead, but my CPU is too old, and it crashes with SIGILL :D

### Other considerations
- In production, GET /tasks should be disabled/removed, as it'd be unusable with a large number of tasks, and may kill performance. I could implement some kind of paging, but there is no reason to do so if nobody needs that endpoint.
