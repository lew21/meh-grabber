#!/bin/sh
wget https://gitlab.com/lew21/waitfor/-/jobs/27129404/artifacts/raw/waitfor -O waitfor
chmod u+x waitfor

docker-compose down
docker-compose up --build -d
docker-compose run -v `pwd`:/home web /home/waitfor -t 180 cassandra:9042
until docker-compose run migrate; do true; done
python3 smoke_test.py
ret=$?
docker-compose down
exit $ret
