#!/bin/sh

# You may want to run this with
# docker run -ti -v `pwd`:/home -w /home python:3.7.1-alpine3.8 sh update-requirements.sh

set -e
python3 -m venv .upenv
. .upenv/bin/activate
pip install --upgrade pip
pip install .
pip uninstall --yes grabber
pip freeze > requirements.txt
rm -Rf .upenv
